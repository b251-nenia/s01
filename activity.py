print("Hello World!")

# 2. Create 5 variables and output in the command prompt in the following format:
name = "Ninz"
age = 24
occupation = "developer"
movie = "Black Panther"
movie_rating = 95.5

print(f"I am {name}, and I am {age} years old, I work as a {occupation}, and my rating for {movie} is {movie_rating} %")

# 3. Create 3 variables, num1, num2, and num3
num1, num2, num3 = 2, 4, 6	
# a. Get the product of num1 and num2
print(num1 * num2)
# b. Check if num1 is less than num3
print(num1 < num3)
# c. Add the value of num3 to num2
print(num3 + num2)


